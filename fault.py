# -*- coding: utf-8 -*-
'''
@file fault.py

@brief This file protects the user from mechanical harm by utilizing overcuurent protection
@details This file utilizes the DRV8847 built in overccurrent prtection circuit and interupts to
protect against current spikes, such as when the motor is "stuck" and pinched agsint and object, such
as the user's finger. When an overcurrent is protected, and interupt is triggered, disabling the nSLEEP
pin and thus both motors. A second interupt is used to reset the fault when the user button is pushed.

@author: Cole and Ethan
'''

import pyb

class nFAULT:
    '''
    @brief A class used to protect against overcurrent and disable motors when it occurs. 
    @details Class used with generic motors connected to a singular Will prompt for a user to bypass the fault 
            before normal function can continue. 
    
    @author: Cole and Ethan
    '''
    
    def __init__ (self, nSLEEP_pin):
        '''
        @brief Creates an nFAULT class given a pre-defined nSLEEP pin.
        @param nSLEEP_pin Pin that can be set to high or low voltage in order to enable and disable motors
        @param fault Boolean that is True if the fault pin is triggered. Used for pressing of the User button
                     to make sure that the board is looking for interaction to reset fault
        '''
        
        # save variables to object:
        self.nSLEEP_pin = nSLEEP_pin
        
        # reset value
        self.fault = False
        
    def enable_fault(self):
        '''
        @brief This function enables the fault for triggering during operation.
        @details This function is primarily used because there was troubles with tripping the fault when the motors were being enables.
                 Something about setting the nSLEEP pin high or low will trigger the nFAULT pin, so this function allows enabling the fault
                 after enabling the motors. 
        '''
        #Fault detection
        self.extint = pyb.ExtInt(pyb.Pin(pyb.Pin.cpu.B2, mode = pyb.Pin.IN),#Setting pin nFAULT_pin to activate an EXTERNAL INTERRUPT
        pyb.ExtInt.IRQ_FALLING,     #Interrupt occurs on falling edge of signal
        pyb.Pin.PULL_NONE,            #Pull up resister is being used for this pin
        self.fault_callback)             #Define what interrupt service routine to go to when activated (fault_callback)
        
        # user button detection
        self.reset = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.reset)
        
    def fault_callback(self, pin):
        '''
        @brief Callback function for when nFAULT is triggered that. Called by self.extint above.
        '''
        print('nFAULT')
        self.extint.disable()
        pyb.delay(100)
        self.nSLEEP_pin.low()
        self.fault = True
        print('Push user button to reset fault!')
    
    def reset(self, pin):
        '''
        @brief Callback function for when User button is pressed after fault. Used to enable motors. If statement is testing if a fault has been triggered.
        '''
        
        if self.fault == True:
            self.nSLEEP_pin.high()
            self.fault = False
            print('Fault reset!')
            pyb.delay(100)
            self.extint.enable()
